#!/usr/bin/env bash
set -o errexit -o nounset -o xtrace

brew install cmake  # for compiling dlib

pip install -r face_detector/app/requirements.txt
pip install -r mis_mm/app/requirements.txt
pip install -r searcher_users/app/requirements.txt


export FACE_DETECTOR_URL=http://localhost:5000
export SEARCHER_USERS_URL=http://localhost:6000

psql -h localhost -p 5432 -U postgres -f searcher_users/init_db.sql
python searcher_users/app/manage.py migrate

FACE_DETECTOR_URL="" SEARCHER_USERS_URL="" python mis_mm/app/manage.py migrate
