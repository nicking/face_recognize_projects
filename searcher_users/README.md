### Сервис поиска похожих пользователей по вектору лица

searching users by face descriptor
```
GET /api/v1/users
    params:  GET
    - descritpror array(float)[128]
    
    result body:
    {items: {'external_id_1': [distances], ...}}
```

add user descriptor
```
POST /api/v1/users
    params:  JSON
    - descritpror array(float)[128]
    - external_id str
    
    result body:
        saved user data
```
(external_id - user id from base system)

### Добавление новых библиотек в requirements.txt
    
    $ pipdeptree -l > requirements.txt

### Запуск тестов

    docker-compose run searcher_users ./wait-for-it.sh searcher_users_db:5432 -- pytest --isort --mypy --flake8

#### Создание базы и пользователей локально

    psql -h localhost -p 5432 -U postgres -f init_db.sql
