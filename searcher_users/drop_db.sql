-- Allows to drop service-related databases and roles
-- Use on local machine only!
DROP DATABASE IF EXISTS searcher;
DROP ROLE IF EXISTS searcher_users;
