from django.conf import settings
from rest_framework import serializers

from user import models


class SearchUserSerializer(serializers.Serializer):
    descriptor = serializers.ListField(
        required=True,
        child=serializers.FloatField(),
        min_length=settings.FACE_VECTOR_SIZE,
        max_length=settings.FACE_VECTOR_SIZE,
    )


class AddUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ('external_id', 'descriptor', 'id')
