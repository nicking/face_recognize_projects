import logging

from django.http import JsonResponse
from rest_framework import generics

from user import finder_users, serializers

logger = logging.getLogger(__name__)


class UserView(generics.ListCreateAPIView):
    serializer_class = serializers.AddUserSerializer

    def list(self, request, *args, **kwargs):
        self.get_serializer()
        serializer = serializers.SearchUserSerializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        users = finder_users.find_similar(
            serializer.validated_data['descriptor'],
            log_extra=request.log_extra,
        )
        return JsonResponse({'items': users})
