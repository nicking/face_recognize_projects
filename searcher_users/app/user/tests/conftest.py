import json
from os import path

import pytest
from django.core import serializers
from rest_framework.test import APIClient

from user import finder_users, models

STATIC_FILES = 'user/tests/static/'


@pytest.fixture
def rest_client():
    return APIClient()


@pytest.fixture
def users(transactional_db):
    """
    Load users from static/users_data.kson

        for serializing to json =>
        >>> from django.core import serializers
        >>> from user import models
        >>> serialized = serializers.serialize('json', models.User.objects.all()[:100])
        >>> with open('user/tests/static/users_data.json', 'w') as f:
        >>>     f.write(serialized)
    """
    fixture = path.join(STATIC_FILES, 'users_data.json')
    with open(fixture) as f:
        for obj in serializers.deserialize('json', f.read()):
            obj.save()
    _users = models.User.objects.all()
    assert _users.count() == 100

    # reload finder users
    finder_users.users_data = finder_users.load_all_faces_lazy()
    yield _users
    finder_users.users_data = finder_users.load_all_faces_lazy()


@pytest.fixture
def nicking_descriptor():
    """
    Vector described Nikita Korolkov face
    There are two photos with this face in users_data.json
    """
    fixture = path.join(STATIC_FILES, 'nicking_descriptor.json')
    with open(fixture) as f:
        return json.loads(f.read())
