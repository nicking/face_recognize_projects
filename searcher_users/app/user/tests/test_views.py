import pytest
from rest_framework import status

from user import models


@pytest.mark.django_db
def test_get_users_from_empty_list(rest_client, nicking_descriptor):
    response = rest_client.get('/api/v1/users', data={
        'descriptor': nicking_descriptor,
    })
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {'items': {}}


def test_get_users(rest_client, users, nicking_descriptor):
    response = rest_client.get('/api/v1/users', data={
        'descriptor': nicking_descriptor,
    })
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {'items': {
        '4948': [0.42891419919401924, 0.5079856003173777],
    }}


@pytest.mark.parametrize('request_body,error', (
    (
        {'descriptor': [1, 2, 3]},
        {'descriptor': ['Ensure this field has at least 128 elements.']},
    ),
    (
        {},
        {'descriptor': ['This field is required.']}
    ),
))
def test_get_users_invalid_request(rest_client, request_body, error):
    response = rest_client.get('/api/v1/users', data=request_body)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == error


@pytest.mark.django_db
@pytest.mark.parametrize('request_body', (
    {'descriptor': [i for i in range(128)], 'external_id': '100500'},
))
def test_create_user(rest_client, request_body):
    response = rest_client.post('/api/v1/users', data=request_body)

    assert response.status_code == status.HTTP_201_CREATED
    user = models.User.objects.get(id=response.json()['id'])
    assert user.external_id == request_body['external_id']
    assert user.descriptor == request_body['descriptor']


@pytest.mark.parametrize('request_body,error', (
    (
        {'descriptor': [1, 2, 3]},
        {
            'descriptor': ['List contains 3 items, it should contain no fewer than 128.'],
            'external_id': ['This field is required.'],
        },
    ),
    (
        {'external_id': 123231},
        {'descriptor': ['This field is required.']}
    ),
))
def test_create_user_invalid_request(rest_client, request_body, error):
    response = rest_client.post('/api/v1/users', data=request_body)

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == error
