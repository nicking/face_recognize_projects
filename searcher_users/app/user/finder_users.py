import bisect
import logging
import typing
from collections import defaultdict

import numpy as np
from django.conf import settings
from django.utils import functional

from user import models

logger = logging.getLogger(__name__)


def find_similar(user_descriptor: typing.List[float],
                 log_extra=None) -> typing.Dict[str, typing.List[float]]:
    user_descriptor = np.array(user_descriptor)
    if not users_data['descriptors'].size:
        return {}

    distances = calculate_distances(users_data['descriptors'], user_descriptor)

    similar_users_indexes = np.where(
        distances < settings.DISTANCE_SIMILAR_FACES
    )[0]  # np.where return matrix with one vector
    logger.info(f'Total similar users=%s; number all descriptors=%s',
                len(similar_users_indexes), len(distances),
                extra=log_extra)

    similar_users = defaultdict(list)  # type: typing.Dict[str, typing.List[float]]
    for index in similar_users_indexes:
        user_external_id = users_data['external_ids'][index]
        user_distances = similar_users[user_external_id]
        bisect.insort(user_distances, distances[index])

    return similar_users


def calculate_distances(all_users_descriptors: np.ndarray,
                        user_descriptor: np.ndarray) -> np.ndarray:
    """Calculate distance for every user's distance"""
    return np.linalg.norm(all_users_descriptors - user_descriptor, axis=1)


def load_all_faces() -> typing.Dict[str, typing.Any]:
    _descriptors = []
    _external_ids = []

    users = models.User.objects.values_list('external_id', 'descriptor')
    for external_id, descriptor in users.iterator(chunk_size=10_000):
        # descriptors.append(np.array(descriptor, dtype=np.float))
        _descriptors.append(np.array(descriptor, dtype=np.float))
        # descriptor.append(descriptor)
        _external_ids.append(external_id)

    return {
        'descriptors': np.array(_descriptors),
        'external_ids': _external_ids,
    }


def load_all_faces_lazy():
    return functional.SimpleLazyObject(lambda: load_all_faces())


if settings.LOAD_DESCRIPTORS:
    users_data = load_all_faces()
else:
    users_data = load_all_faces_lazy()
