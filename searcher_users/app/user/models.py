from django.conf import settings
from django.contrib.postgres import fields as pg_fields
from django.contrib.postgres import validators
from django.db import models


class User(models.Model):
    external_id = models.CharField(
        max_length=30, verbose_name="User's identificator from another system",
    )
    descriptor = pg_fields.ArrayField(
        base_field=models.FloatField(),
        size=settings.FACE_VECTOR_SIZE,
        validators=[validators.ArrayMinLengthValidator(settings.FACE_VECTOR_SIZE)],
        verbose_name='Numbers whose describes user face',
    )
