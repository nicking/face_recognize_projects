import logging
import uuid
from datetime import datetime

from django.urls import resolve
from django.utils.deprecation import MiddlewareMixin

logger = logging.getLogger(__name__)


class SetLogExtraMiddleware(MiddlewareMixin):
    """
    Middleware that set request_id
    """
    LOGGING_FORMAT = ('%(levelname)s %(asctime)s %(module)s '
                      'message="%(message)s" '
                      'request_id=%(request_id)s '
                      'method=%(method)s '
                      'path=%(path)s'
                      )
    HEADER_REQUEST_ID = 'Request-Id'

    def process_request(self, request):
        request_id = request.headers.get(self.HEADER_REQUEST_ID,
                                         uuid.uuid4().hex)
        request.log_extra = {
            'request_id': request_id,
            'method': request.method,
            'path': resolve(request.path_info).route,
        }

    def process_response(self, request, response):
        return response


class LoggingRequestMiddleware(MiddlewareMixin):
    """
    Middleware
    required set after SetLogExtraMiddleware
    """

    def process_request(self, request):
        request.start_time = datetime.now()
        logger.info('Request start', extra=request.log_extra)

    def process_response(self, request, response):
        end_time = datetime.now() - request.start_time

        body = '_'
        if response.status_code >= 400:
            body = response.content
        logger.info('Response time=%s code=%s; %s',
                    end_time, response.status_code, body,
                    extra=request.log_extra)
        return response
