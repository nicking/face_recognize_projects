-- Creates initial database and required roles

CREATE ROLE searcher WITH LOGIN PASSWORD '123';

CREATE DATABASE searcher_users WITH
    TEMPLATE = template0
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8';

ALTER DATABASE searcher_users OWNER TO searcher;
