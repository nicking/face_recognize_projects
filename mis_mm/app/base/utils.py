import logging
import typing

from django.core.files import uploadedfile
from requests import exceptions

from base import clients, models, serializers

logger = logging.getLogger(__name__)


def get_users_data(found_similar_users: typing.Dict,
                   ) -> typing.List[typing.Dict]:
    """
    Get similar users from DB
      and mix their distances
    """
    users = models.User.objects.filter(
        id__in=found_similar_users.keys(),
    )
    users_data = serializers.UserSerializer(users, many=True).data
    for user in users_data:
        user['distance'] = found_similar_users[str(user['id'])]

    return sorted(users_data, key=lambda u: u['distance'][0])


def find_face_by_photo(photo: uploadedfile.InMemoryUploadedFile,
                       log_extra=None,
                       **kwargs) -> typing.Optional[typing.Dict]:
    photo = typing.cast(typing.IO, photo)
    try:
        return clients.FaceDetectorClient().find_face(
            photo_file=photo,
            log_extra=log_extra,
            **kwargs,
        )
    except exceptions.HTTPError as e:
        if e.response.status_code != 400:
            logger.exception('clients.FaceDetectorClient got exception',
                             extra=log_extra)
            raise
        return None
