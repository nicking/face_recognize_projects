import logging
import os

import tqdm as tqdm
from django.core.files import File, storage
from django.core.management.base import BaseCommand, CommandParser
from requests import exceptions

from base import clients, models

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    detector = clients.FaceDetectorClient()
    users_descriptors_storage = clients.SearcherUsersClient()
    photo_storage = storage.FileSystemStorage()

    def add_arguments(self, parser: CommandParser):
        parser.add_argument(
            '-d',
            '--directory',
            dest='directory',
            help='Users photos directory',
        )
        parser.add_argument(
            '--file_remove',
            dest='file_remove',
            help='Success added photo will be remove',
            default=False,
            type=lambda v: v.lower() in ['true', 't', 'yes', 'y'],
        )

    def handle(self, *args, **options):
        self.find_and_add_users(options['directory'], with_file_remove=options['file_remove'])

    def find_and_add_users(self, directory, with_file_remove=False):
        logger.info('Directory=%s', directory, extra={
            'request_id': '',
            'path': '-',
            'method': '-',
        })
        for index, file in tqdm.tqdm(enumerate(sorted(os.listdir(directory)))):
            path = os.path.join(directory, file)

            if os.path.isdir(path):
                self.find_and_add_users(path, with_file_remove=with_file_remove)
                continue

            self.add_user(
                path,
                with_file_remove,
                log_extra={
                    'request_id': f'ADD_USER_COMMAND_{index}',
                    'path': '-',
                    'method': '-',
                },
            )

    def add_user(self, file_path, with_file_remove: bool, log_extra=None):
        with open(file_path, 'rb') as f:
            try:
                detected_face = self.detector.find_face(
                    photo_file=f, with_frame=False, log_extra=log_extra,
                )
            except exceptions.HTTPError as e:
                logger.warning('Can not detect face=%s; exc=%s', file_path, e,
                               extra=log_extra)
                return

            file_name = models.get_photo_path(None, os.path.basename(f.name))
            user = models.User.objects.create(
                photo=self.photo_storage.save(
                    name=file_name,
                    content=File(f),
                ),
                username=file_name,
            )
            self.users_descriptors_storage.add(
                descriptor=detected_face['descriptor'],
                user_id=user.id,
                log_extra=log_extra,
            )

        if with_file_remove:
            os.remove(file_path)
