import logging
import typing

from django import views
from django.db.transaction import atomic
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from rest_framework import status

from base import clients, forms, models, utils

logger = logging.getLogger(__name__)


def index(request):
    return render(request, 'index.html')


def detect_user_from_camera(request):
    detected_face_data = clients.FaceDetectorClient().detect_face(
        with_frame=True, log_extra=request.log_extra,
    )
    found_similar_users = clients.SearcherUsersClient().find(
        descriptor=detected_face_data['descriptor'],
        log_extra=request.log_extra,
    )

    return JsonResponse(
        data={
            'items': utils.get_users_data(found_similar_users),
            'descriptor': detected_face_data['descriptor'],
            'frame': detected_face_data['frame'],
        },
    )


def detect_user_by_photo(request):
    form = forms.SearchUsersByPhotoForm(files=request.FILES)
    if not form.is_valid():
        return JsonResponse(
            {'errors': ['photo was not found']},
            status=status.HTTP_400_BAD_REQUEST,
        )

    found_face_data = utils.find_face_by_photo(
        form.cleaned_data['photo'],
        with_frame=True,
        log_extra=request.log_extra,
    )
    if not found_face_data:
        return JsonResponse(
            {'errors': ['face was not found']},
            status=status.HTTP_400_BAD_REQUEST,
        )

    found_similar_users = clients.SearcherUsersClient().find(
        descriptor=found_face_data['descriptor'],
        log_extra=request.log_extra,
    )

    return JsonResponse(
        data={
            'items': utils.get_users_data(found_similar_users),
            'descriptor': found_face_data['descriptor'],
            'frame': found_face_data['frame'],
        },
    )


class CreateUserView(views.generic.CreateView):
    template_name = 'create_user.html'
    model = models.User
    fields = ('photo', 'username')
    success_url = reverse_lazy('find_users')

    @atomic
    def form_valid(self, form):
        found_face_data = utils.find_face_by_photo(
            photo=form.cleaned_data['photo'],
            log_extra=self.request.log_extra,
        )
        if not found_face_data:
            form.add_error('photo', 'Лицо не найдено')
            return self.form_invalid(form)

        response = super().form_valid(form)
        face_descriptor = typing.cast(
            typing.List[float], found_face_data['descriptor'],
        )
        self.add_user_descriptor(face_descriptor, user_id=self.object.id)
        return response

    def add_user_descriptor(self, descriptor: typing.List[float],
                            user_id: int):
        clients.SearcherUsersClient().add(
            descriptor=descriptor,
            user_id=user_id,
            log_extra=self.request.log_extra,
        )
