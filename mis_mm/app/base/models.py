from django.db import models


def get_photo_path(_, filename: str) -> str:
    if not filename:
        filename = 'noname'
    file_dir = filename[0].lower()
    return f'photos/{file_dir}/{filename}'


class User(models.Model):
    username = models.CharField(max_length=255)
    photo = models.ImageField(upload_to=get_photo_path)
