from django.urls import path

from base import views

urlpatterns = [
    path('', views.index, name='find_users'),
    path('api/v1/users/similar/camera', views.detect_user_from_camera),
    path('api/v1/users/similar/photo', views.detect_user_by_photo),
    path('users/create', views.CreateUserView.as_view(), name='add_user'),
]
