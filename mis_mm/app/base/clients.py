import logging
import typing
from urllib.parse import urljoin

import requests
from django.conf import settings

from project import middleware

logger = logging.getLogger(__name__)


class BaseClient:
    url = None
    request_id_header = middleware.SetLogExtraMiddleware.HEADER_REQUEST_ID

    def request(self, method: str, path: str,
                params: typing.Optional[dict] = None,
                json: typing.Optional[dict] = None,
                files: typing.Optional[dict] = None,
                data: typing.Optional[dict] = None,
                timeout: int = settings.DEFAULT_REQUEST_TIMEOUT,
                log_extra: typing.Optional[dict] = None):

        client_name = type(self).__name__
        full_path = urljoin(self.url, path)
        logger.info(
            '%s make request to url=%s; method=%s',
            client_name, full_path, method,
            extra=log_extra,
        )
        headers = {'HOST': 'mismm'}
        if log_extra and 'request_id' in log_extra:
            headers[self.request_id_header] = log_extra['request_id']

        response = None
        try:
            response = requests.request(
                method=method,
                url=full_path,
                params=params,
                json=json,
                files=files,
                data=data,
                timeout=timeout,
                headers=headers,
            )

            response.raise_for_status()
        except requests.RequestException:
            response_body = '' if response is None else response.content
            logger.exception(
                '%s got bad request to url=%s; response_body="%s"',
                client_name, full_path, response_body,
                extra=log_extra,
            )
            raise

        body = response.json()
        logger.info(
            '%s got success request url=%s',
            client_name, full_path, extra=log_extra,
        )
        return body

    def get(self, path: str, **kwargs):
        return self.request('get', path, **kwargs)

    def post(self, path: str, **kwargs):
        return self.request('post', path, **kwargs)


class FaceDetectorClient(BaseClient):
    url = settings.FACE_DETECTOR_URL

    def detect_face(self, with_frame: bool = False, gray_frame: bool = False,
                    size: int = 300,
                    log_extra=None):
        return self.post(
            '/api/v1/face/detect',
            json={
                'with_frame': with_frame,
                'gray_frame': gray_frame,
                'size': size,
            },
            log_extra=log_extra,
        )

    def find_face(self, photo_file: typing.IO,
                  with_frame: bool = False,
                  gray_frame: bool = False,
                  size=300, log_extra=None):
        return self.post(
            '/api/v1/face/find',
            files={
                'photo': photo_file,
            },
            data={
                'with_frame': with_frame,
                'gray_frame': gray_frame,
                'size': size,
            },
            log_extra=log_extra,
        )


class SearcherUsersClient(BaseClient):
    url = settings.SEARCHER_USERS_URL

    def find(self, descriptor, log_extra=None):
        return self.get(
            '/api/v1/users',
            params={
                'descriptor': descriptor,
            },
            log_extra=log_extra,
        )['items']

    def add(self, descriptor, user_id, log_extra=None):
        return self.post(
            '/api/v1/users',
            json={
                'descriptor': descriptor,
                'external_id': user_id,
            },
            log_extra=log_extra,
        )
