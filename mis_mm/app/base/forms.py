from django import forms


class SearchUsersByPhotoForm(forms.Form):
    photo = forms.ImageField()
