### Сервис захвата изображения с лицом и нахожденим его вектора (descriptor)

for detecting face from camera
```
POST /api/v1/face/detect
    params:  json
    - with_frame [bool]
    - gray_frame [bool]
    - size [int] (max_size=600)
    
    result body:
    {descriptor: [128 numbers], frame: 'img in base64'}
```

for finding face by photo
```
POST /api/v1/face/find
    params:  form data
    - photo [multipart form data]
    - with_frame [bool]
    - gray_frame [bool]
    - size [int] (max_size=600)
    
    result body:
    {descriptor: [128 numbers], frame: 'img in base64'}
```

##### Использование более сложного поиска лиц
    
При необходимости можно изменить поиск лиц с hog на сверточные сети
установив в environments `USE_SIMPLE_DETECTING=False` 
   

##### Добавление новых библиотек в requirements.txt
    
    $ pipdeptree -l > requirements.txt

#### Запуск тестов:

    docker build -t face_detector .
    docker run -it face_detector pytest --isort --mypy --flake8 -vv

    $ pytest --isort --mypy --flake8 -vv
