import logging
import typing

import rest_framework.request
from django.http import JsonResponse
from rest_framework import exceptions
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import MultiPartParser

from detecting import detector
from face import serializers, utils
from face.serializers import DetectFaceSerializer

logger = logging.getLogger(__name__)


@api_view(['POST'])
def detect_face_view(request: rest_framework.request.Request) -> JsonResponse:
    serializer = DetectFaceSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    detected_face = detector.get_face_description()
    logger.info('Face was found', extra=request.log_extra)
    return JsonResponse(utils.get_response_body(detected_face, serializer))


@api_view(['POST'])
@parser_classes([MultiPartParser])
def find_face_view(request: rest_framework.request.Request) -> JsonResponse:
    serializer = serializers.FindFaceSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    frame = utils.convert_to_opencv(serializer.validated_data['photo'])
    logger.info('Success converted frame', extra=request.log_extra)
    detected_face = detector.get_face_descriptor(frame)

    if detected_face is None:
        logger.info('Face was not found', extra=request.log_extra)
        raise exceptions.ValidationError({'errors': ['face was not found']})

    logger.info('Face was found', extra=request.log_extra)
    serializer = typing.cast(serializers.DetectFaceSerializer, serializer)
    return JsonResponse(utils.get_response_body(detected_face, serializer))
