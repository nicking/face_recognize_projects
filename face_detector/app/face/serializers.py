from django.conf import settings
from rest_framework import serializers


class DetectFaceSerializer(serializers.Serializer):
    with_frame = serializers.BooleanField(default=False, required=False)
    gray_frame = serializers.BooleanField(default=False, required=False)
    size = serializers.IntegerField(required=False,
                                    max_value=settings.MAX_FACE_SIZE,
                                    min_value=0,
                                    default=settings.MAX_FACE_SIZE)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if attrs['gray_frame'] and not attrs['with_frame']:
            raise serializers.ValidationError({
                'errors': 'You should not set gray_size=True '
                          'while with_frame is False'
            })
        return attrs


class FindFaceSerializer(DetectFaceSerializer):
    photo = serializers.ImageField(use_url=False, required=True)
