from django.urls import path

from face import views

urlpatterns = [
    path('api/v1/face/detect', views.detect_face_view),
    path('api/v1/face/find', views.find_face_view),
]
