import base64
import typing

import cv2
import numpy as np
import PIL.Image
from django.core.files import uploadedfile

from detecting import detector
from face import serializers


def get_response_body(
        detected_face: detector.DetectedFace,
        serializer: serializers.DetectFaceSerializer
) -> typing.Dict[str, typing.Any]:
    response_body = {
        'descriptor': list(detected_face.descriptor),
    }  # type: typing.Dict[str, typing.Any]

    debug_image = get_debug_frame(detected_face, serializer)
    if debug_image:
        response_body['frame'] = debug_image
    return response_body


def get_debug_frame(detected_face: detector.DetectedFace,
                    serializer: serializers.DetectFaceSerializer,
                    ) -> typing.Optional[str]:
    data = serializer.validated_data
    if not data['with_frame']:
        return None
    frame = detector.export_debug_image(
        detected_face, to_gray=data['gray_frame'], size=data['size'],
    )
    return base64.b64encode(frame).decode(encoding='utf-8')


def convert_to_opencv(image: uploadedfile.InMemoryUploadedFile) -> np.ndarray:
    im = PIL.Image.open(image).convert('RGB')
    return cv2.cvtColor(np.array(im), cv2.COLOR_BGR2RGB)
    # return np.array(im)
