from django.apps import AppConfig


class SearchFaceConfig(AppConfig):
    name = 'search_face'
