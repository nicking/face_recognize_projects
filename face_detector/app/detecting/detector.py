import atexit
import typing
from dataclasses import dataclass

import cv2
import dlib
import face_recognition_models
import imutils
import numpy as np
from django.conf import settings

_camera: typing.Optional[cv2.VideoCapture] = None

face_detector = dlib.get_frontal_face_detector()  # hog face detector
cnn_face_detector = dlib.cnn_face_detection_model_v1(
    face_recognition_models.cnn_face_detector_model_location()
)  # detector on convolutional neural network
face_landmark_predictor = dlib.shape_predictor(
    face_recognition_models.pose_predictor_model_location()
)  # trees
face_encoder = dlib.face_recognition_model_v1(
    face_recognition_models.face_recognition_model_location()
)  # ResNet-34


@dataclass
class DetectedFace:
    frame: np.ndarray
    rectangle: dlib.rectangle
    landmark_points: dlib.full_object_detection
    descriptor: dlib.vector


def get_face_description(camera_id: int = 0) -> DetectedFace:
    cam = get_camera(camera_id)
    while True:
        ret, frame = cam.read()
        if not ret:
            continue

        detected_face = get_face_descriptor(frame)
        if not detected_face:
            continue
        return detected_face


def get_camera(camera_id: int) -> cv2.VideoCapture:
    global _camera
    if _camera is None:
        _camera = cv2.VideoCapture(camera_id)
        _camera = typing.cast(cv2.VideoCapture, _camera)
        atexit.register(lambda: _camera.release())  # type: ignore
    return _camera


def get_face_descriptor(frame: np.ndarray) -> typing.Optional[DetectedFace]:
    frame = imutils.resize(frame, width=settings.MAX_FACE_SIZE)

    face_rectangle = get_face_rectangle(frame)
    if face_rectangle is None:
        return None

    # found 68 points described face
    landmark_points = face_landmark_predictor(frame, face_rectangle)

    face_descriptor = face_encoder.compute_face_descriptor(
        frame, landmark_points,
    )

    return DetectedFace(
        frame=frame,
        rectangle=face_rectangle,
        landmark_points=landmark_points,
        descriptor=face_descriptor,
    )


def get_face_rectangle(frame: np.ndarray) -> typing.Optional[dlib.rectangle]:
    if settings.USE_SIMPLE_DETECTING:
        return get_simple_face_rectangle(frame)
    return get_more_accuracy_face_rectangle(frame)


def get_simple_face_rectangle(frame: np.ndarray) -> typing.Optional[dlib.rectangle]:
    face_rectangles = face_detector(frame, 1)
    if not face_rectangles:
        return None

    face_rectangles = sorted(
        face_rectangles, key=get_rectangle_size, reverse=True,
    )
    return face_rectangles[0]


def get_more_accuracy_face_rectangle(frame: np.ndarray) -> typing.Optional[dlib.rectangle]:
    """
    Get biggest face rectangle

    for more accuracy we use 2 detector with HOG+SVM and CNN detectors
     HOG+SVM - is very fast algorithm, but often has False Positive
     CNN - more accuracy algorithm, but very slow
    """

    # How many times to upsample the image looking for faces.
    # Higher numbers find smaller faces.
    numbers_of_times_to_upsample = 0

    # fasted searcher faces
    if not face_detector(frame, numbers_of_times_to_upsample):
        return None

    # more accuracy searcher
    face_rectangles: dlib.mmod_rectangle = cnn_face_detector(
        frame, numbers_of_times_to_upsample,
    )
    if not face_rectangles:
        return None

    face_rectangles = sorted(
        face_rectangles, key=get_mmod_rectangle_size, reverse=True,
    )
    return face_rectangles[0].rect


def get_mmod_rectangle_size(mmod_rectangle: dlib.mmod_rectangle) -> float:
    return get_rectangle_size(mmod_rectangle.rect)


def get_rectangle_size(r: dlib.rectangle) -> float:
    return (r.right() - r.left()) * (r.bottom() - r.top())



def draw_landmark_on_face(detected_face: DetectedFace) -> np.ndarray:
    frame = detected_face.frame.copy()
    white_color = (255, 255, 255)

    # draw border of the biggest face
    rect = detected_face.rectangle
    cv2.rectangle(
        frame,
        pt1=(rect.left(), rect.top()),
        pt2=(rect.right(), rect.bottom()),
        color=white_color,
        thickness=2,
    )

    # draw landmark points
    for point_index in range(detected_face.landmark_points.num_parts):
        point = detected_face.landmark_points.part(point_index)
        cv2.circle(
            frame,
            center=(point.x, point.y),
            radius=1,
            color=white_color,
            thickness=1,
        )
    return frame


def export_debug_image(detected_face: DetectedFace, **kwargs) -> bytes:
    frame = export_debug_frame(detected_face, **kwargs)
    _, encoded_image = cv2.imencode('.png', frame)
    return encoded_image.tobytes()


def export_debug_frame(
        detected_face: DetectedFace,
        to_gray: bool,
        size: int) -> np.ndarray:
    frame = draw_landmark_on_face(detected_face)
    frame = imutils.resize(frame, width=size, height=size)
    if to_gray:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    return frame


if __name__ == '__main__':
    face_data = get_face_description()

    debug_frame = export_debug_frame(face_data, to_gray=False, size=400)
    cv2.imwrite('detected_face_debug.png', debug_frame)

    frame_with_landmark = draw_landmark_on_face(face_data)
    cv2.namedWindow('Output')
    cv2.imshow("Output", frame_with_landmark)
    atexit.register(lambda: cv2.destroyAllWindows())
    cv2.imwrite('detected_face.png', frame_with_landmark)
    cv2.waitKey(0)
