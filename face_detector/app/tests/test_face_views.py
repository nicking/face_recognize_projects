import pytest
from django.conf import settings
from rest_framework import status


@pytest.mark.parametrize('request_body,with_frame', (
    ({}, False),
    ({'with_frame': True}, True),
    ({'with_frame': True, 'gray_frame': True}, True),
    ({'with_frame': True, 'size': 300}, True),
))
def test_detect_face(rest_client, get_camera_mock, request_body, with_frame):
    response = rest_client.post('/api/v1/face/detect', request_body,
                                format='json')
    assert response.status_code == status.HTTP_200_OK
    body = response.json()
    assert body['descriptor']
    if with_frame:
        assert body['frame']
    else:
        assert 'frame' not in body


@pytest.mark.parametrize('request_body,error', (
    (
        {'with_frame': False, 'gray_frame': True},
        {'errors': ['You should not set gray_size=True '
                    'while with_frame is False']}),
    (
        {'with_frame': 'True', 'size': settings.MAX_FACE_SIZE + 100},
        {'size': ['Ensure this value is less than or equal to 600.']},
    ),
))
def test_detect_face_invalid_params(rest_client, request_body, error):
    response = rest_client.post('/api/v1/face/detect', request_body,
                                format='json')
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == error


@pytest.mark.parametrize('request_body,with_frame', (
    ({}, False),
    ({'with_frame': True}, True),
    ({'with_frame': True, 'gray_frame': True}, True),
    ({'with_frame': True, 'size': 300}, True),
))
def test_find_face(client, user_photo_path, request_body, with_frame):
    with open(user_photo_path, 'rb') as f:
        response = client.post('/api/v1/face/find',
                               {'photo': f, **request_body})

    assert response.status_code == status.HTTP_200_OK
    body = response.json()
    assert body['descriptor']
    if with_frame:
        assert body['frame']
    else:
        assert 'frame' not in body


def test_find_face_not_found(client, not_user_photo_path):
    with open(not_user_photo_path, 'rb') as f:
        response = client.post('/api/v1/face/find', {'photo': f})

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {'errors': ['face was not found']}
