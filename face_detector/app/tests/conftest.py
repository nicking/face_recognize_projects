from unittest import mock

import cv2
import numpy as np
import pytest
import pytest_mock
import rest_framework.test


@pytest.fixture
def rest_client():
    return rest_framework.test.APIClient()


@pytest.fixture(scope='session')
def user_photo_path():
    return 'tests/static/Arantxa_Sanchez_Vicario.jpg'


@pytest.fixture(scope='session')
def not_user_photo_path():
    return 'tests/static/not_user_face.jpg'


@pytest.fixture(scope='session')
def frame(user_photo_path):
    return cv2.imread(user_photo_path)


@pytest.fixture
def get_camera_mock(mocker: pytest_mock.MockFixture, frame: np.ndarray):
    camera = mock.Mock(**{
        'read.return_value': (True, frame),
    })
    mocked_camera = mocker.patch(
        'detecting.detector.get_camera', return_value=camera,
    )
    yield camera
    mocked_camera.assert_called_once()
    camera.read.assert_called_once()


@pytest.fixture
def mock_accuracy_detecting(mocker: pytest_mock.MockFixture):
    """
    Set more accuracy searcher faces
    """
    mocker.patch(
        'project.settings.USE_SIMPLE_DETECTING', False,
    )
