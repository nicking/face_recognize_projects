import os

from project.middleware import SetLogExtraMiddleware

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '1bzm34c!gd5593-)(f5#=v)m8_1s6#$z_q)hfho(mldh8kx8+5'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get('DEBUG', 'True').lower() == 'true'


ALLOWED_HOSTS = ['mismm', 'localhost', '0.0.0.0']


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'face',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'project.middleware.SetLogExtraMiddleware',
    'project.middleware.LoggingRequestMiddleware',
]

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'project.wsgi.application'

LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            'format': SetLogExtraMiddleware.LOGGING_FORMAT,
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'default': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'face': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'detecting': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'project': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'django': {
            'handlers': ['default'],
            'level': 'INFO',
        }
    }
}

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = '/static/'

MAX_FACE_SIZE = 600
USE_SIMPLE_DETECTING = os.environ.get('USE_SIMPLE_DETECTING', 'true').lower() == 'true'
