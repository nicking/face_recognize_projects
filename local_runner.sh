#!/usr/bin/env bash
set -o nounset -o xtrace

MIS_MM_PORT=8000
FACE_DETECTOR_PORT=5000
SEARCHER_USERS_PORT=6000


function kill_apps {
    echo "kill runned apps"
    kill $(ps -A | grep ":$MIS_MM_PORT" | awk '{print $1}')
    kill $(ps -A | grep ":$FACE_DETECTOR_PORT" | awk '{print $1}')
    kill $(ps -A | grep ":$SEARCHER_USERS_PORT" | awk '{print $1}')
}
kill_apps
trap kill_apps EXIT


python face_detector/app/manage.py runserver "0.0.0.0:$FACE_DETECTOR_PORT" &

python searcher_users/app/manage.py runserver "0.0.0.0:$SEARCHER_USERS_PORT" &

FACE_DETECTOR_URL="http://localhost:$FACE_DETECTOR_PORT" \
SEARCHER_USERS_URL="http://localhost:$SEARCHER_USERS_PORT" \
    python mis_mm/app/manage.py runserver "0.0.0.0:$MIS_MM_PORT"
