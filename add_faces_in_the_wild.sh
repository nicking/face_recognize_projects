#!/usr/bin/env bash
set -o errexit -o nounset -o xtrace

FACE_DETECTOR_PORT=5000
SEARCHER_USERS_PORT=6000

[[ -d faces ]] || mkdir faces
cd faces
if [[ ! -d lfw ]]; then
    brew install wget
    wget http://vis-www.cs.umass.edu/lfw/lfw.tgz
    tar zxvf lfw.tgz
    rm lfw.tgz
fi
FACES_DIR="$PWD/lfw"
cd ..

FACE_DETECTOR_URL="http://localhost:$FACE_DETECTOR_PORT" \
SEARCHER_USERS_URL="http://localhost:$SEARCHER_USERS_PORT" \
python mis_mm/app/manage.py add_users --directory="$FACES_DIR" --file_remove=True
