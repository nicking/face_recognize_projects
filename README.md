### Сервисы поиска людей по снятию лица

![alt create_photo_vector](create_photo_vector.png)
![alt add_user_photo](add_user_photo.png)
![alt search_similar_users](search_similar_users.png)


### Для запуска проектов 

    docker-compose run searcher_users \
        ./wait-for-it.sh searcher_users_db:5432 -- python manage.py migrate
    docker-compose up  # (face_detector - камера в докере не работает)
    
### для запуска локально следует установить postgres

    docker volume create postgres-data

    docker run -p 127.0.0.1:5432:5432 --name postgres \
     -e POSTGRES_PASSWORD=postgres \
     --network-alias=database \
     --mount source=postgres-data,destination=/var/lib/postgresql/data
     -d postgres:11 -c log_statement=all -c log_min_duration_statement=0   
 

### или локально (предварительно создав виртуальное окружение и войдя в него)
    
    ./initial_setup.sh  # устанавливает зависимости, создает базы и мигрирует
    ./local_runner.sh  # запускает 3 сервиса
    
    Можно переходить на http://0.0.0.0:8000/
    
- так же можно добавить wild пользователей (15000 рыл из инета)

      ./add_faces_in_the_wild.sh  # при работающих сервисах

### Для запуска тестов
    
    docker-compose run searcher_users ./wait-for-it.sh searcher_users_db:5432 \ 
        -- pytest --isort --mypy --flake8
    docker-compose run face_detector pytest --isort --mypy --flake8
  